﻿using Microsoft.ServiceBus.Messaging;
using MS.Tenants.WebService.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Configuration;
using System.Web.Http;

namespace MS.Tenants.WebService.Controllers
{
    public class BaseController : ApiController
    {
        public Guid GetEnvironment()
        {
            IEnumerable<string> environments;
            if (Request.Headers.TryGetValues("environmentId", out environments))
            {
                string enviornmentId = environments.First();
                return Guid.Parse(enviornmentId);
            }
            throw new HttpResponseException(HttpStatusCode.InternalServerError);
        }

        public void Log(Guid enviornmentId, string message, string userName = "")
        {
            // TODO: get this from KeyVault
            var connectionString = WebConfigurationManager.AppSettings["sb:LoggingConnectionString"];
            var hubName = WebConfigurationManager.AppSettings["sb:LoggingHubName"];

            try
            {
                var eventHubClient = EventHubClient.CreateFromConnectionString(connectionString, hubName);

                var logEvent = new LogEventEntity()
                {
                    Timestamp = DateTime.UtcNow.ToString(),
                    Message = message,
                    Environment = enviornmentId.ToString(),
                    Machine = Environment.MachineName
                };

                eventHubClient.Send(new EventData(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(logEvent))));
            }
            catch
            {
                Console.Write("Call to logger event hub failed");
            }
        }
    }
}