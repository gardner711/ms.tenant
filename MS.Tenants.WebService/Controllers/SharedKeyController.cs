﻿using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using MS.Tenants.WebService.Entities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using WAOnline.Tenants.WebService.Models;

namespace MS.Tenants.WebService.Controllers
{
    [RoutePrefix("tenants/{tenantId:Guid}/sharedkeys")]
    public class SharedKeyController : BaseController
    {
        CloudTable _sharedkeysTable;

        public SharedKeyController()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting("StorageConnectionString"));
            CloudTableClient client = storageAccount.CreateCloudTableClient();
            _sharedkeysTable = client.GetTableReference("sharedkeys");
        }



        [HttpGet]
        [Route("")]
        public IEnumerable<SharedKeyModel> GetSharedKeys(Guid tenantId, string key = "")
        {
            Guid environmentId = GetEnvironment();
            var ret = new List<SharedKeyModel>();
            try
            {
                string partitionKey = environmentId.ToString() + "-" + tenantId.ToString();
                if (!string.IsNullOrEmpty(key))
                {
                    TableQuery<SharedKeyEntity> rangeQuery = new TableQuery<SharedKeyEntity>().Where(
                        TableQuery.CombineFilters(
                            TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionKey),
                            TableOperators.And,
                            TableQuery.GenerateFilterCondition("key", QueryComparisons.Equal, key)));
                    foreach (SharedKeyEntity entity in _sharedkeysTable.ExecuteQuery(rangeQuery))
                    {
                        ret.Add(new SharedKeyModel(entity));
                    }
                }
                else
                {
                    TableQuery<SharedKeyEntity> rangeQuery = new TableQuery<SharedKeyEntity>().Where(
                        TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionKey));
                    foreach (SharedKeyEntity entity in _sharedkeysTable.ExecuteQuery(rangeQuery))
                    {
                        ret.Add(new SharedKeyModel(entity));
                    }
                }
            }
            catch (Exception ex)
            {
                Log(environmentId, ex.Message);
                throw;
            }
            return ret;
        }

        [HttpPost]
        [Route("")]
        public Guid PostSharedKey(Guid tenantId, [FromBody] SharedKeyModel sharedKey)
        {
            Guid environmentId = GetEnvironment();
            Guid ret = Guid.Empty;
            try
            {
                sharedKey.Id = Guid.NewGuid();
                SharedKeyEntity sharedKeyEntity = new SharedKeyEntity(environmentId, tenantId, sharedKey);
                TableOperation insertOperation = TableOperation.Insert(sharedKeyEntity);
                _sharedkeysTable.Execute(insertOperation);
                ret = sharedKey.Id;
            }
            catch (Exception ex)
            {
                Log(environmentId, ex.Message);
                throw;
            }
            return ret;
        }

        [HttpGet()]
        [Route("{sharedKeyId:Guid}")]
        public SharedKeyModel GetSharedKey(Guid tenantId, Guid sharedKeyId)
        {
            Guid environmentId = GetEnvironment();
            SharedKeyModel ret = null;
            try
            {
                TableOperation retrieveOperation = TableOperation.Retrieve<SharedKeyEntity>(environmentId.ToString() + "-" + tenantId.ToString(), sharedKeyId.ToString());
                TableResult result = _sharedkeysTable.Execute(retrieveOperation);
                if (result.Result == null)
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                SharedKeyEntity sharedKeyEntity = (SharedKeyEntity)result.Result;
                ret = new SharedKeyModel(sharedKeyEntity);
            }
            catch (Exception ex)
            {
                Log(environmentId, ex.Message);
                throw;
            }
            return ret;
        }

        [HttpPatch()]
        [Route("{sharedKeyId:Guid}")]
        public SharedKeyModel PatchTenant(Guid tenantId, Guid sharedKeyId, [FromBody] IDictionary<string, JToken> arguments)
        {
            Guid environmentId = GetEnvironment();
            SharedKeyModel ret = null;
            try
            {
                TableOperation retrieveOperation = TableOperation.Retrieve<SharedKeyEntity>(environmentId.ToString() + "-" + tenantId.ToString(), sharedKeyId.ToString());
                TableResult result = _sharedkeysTable.Execute(retrieveOperation);
                if (result.Result == null)
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                SharedKeyEntity sharedKeyEntity = (SharedKeyEntity)result.Result;
                Dictionary<string, JToken> args = new Dictionary<string, JToken>(arguments, StringComparer.CurrentCultureIgnoreCase);
                JToken value = null;
                if (args.TryGetValue("name", out value))
                    sharedKeyEntity.Name = value.Value<string>();
                value = null;
                if (args.TryGetValue("description", out value))
                    sharedKeyEntity.Description = value.Value<string>();
                value = null;
                if (args.TryGetValue("disabled", out value))
                    sharedKeyEntity.Disabled = value.Value<bool>();
                TableOperation replaceOperation = TableOperation.Replace(sharedKeyEntity);
                _sharedkeysTable.Execute(replaceOperation);
                ret = new SharedKeyModel(sharedKeyEntity);
            }
            catch (Exception ex)
            {
                Log(environmentId, ex.Message);
                throw;
            }
            return ret;
        }
    }
}