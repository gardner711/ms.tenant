﻿using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using MS.Tenants.WebService.Controllers;
using MS.Tenants.WebService.Entities;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using WAOnline.Tenants.WebService.Models;

namespace WAOnline.Tenants.WebService.Controllers
{
    [RoutePrefix("tenants")]
    public class TenantsController : BaseController
    {
        CloudTable _tenantsTable;

        public TenantsController()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting("StorageConnectionString"));
            CloudTableClient client = storageAccount.CreateCloudTableClient();
            _tenantsTable = client.GetTableReference("tenants");
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<TenantModel> GetTenants(string urlFriendlyName = "")
        {
            Guid environmentId = GetEnvironment();
            var ret = new List<TenantModel>();
            try
            {
                if (!string.IsNullOrEmpty(urlFriendlyName))
                {
                    TableQuery<TenantEntity> rangeQuery = new TableQuery<TenantEntity>().Where(
                        TableQuery.CombineFilters(
                            TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, environmentId.ToString()),
                            TableOperators.And,
                            TableQuery.GenerateFilterCondition("UrlFriendlyName", QueryComparisons.Equal, urlFriendlyName)));
                    foreach (TenantEntity entity in _tenantsTable.ExecuteQuery(rangeQuery))
                    {
                        ret.Add(new TenantModel(entity));
                    }
                }
                else
                {
                    TableQuery<TenantEntity> rangeQuery = new TableQuery<TenantEntity>().Where(
                        TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, environmentId.ToString()));
                    foreach (TenantEntity entity in _tenantsTable.ExecuteQuery(rangeQuery))
                    {
                        ret.Add(new TenantModel(entity));
                    }
                }
            }
            catch (Exception ex)
            {
                Log(environmentId, ex.Message);
                throw;
            }
            return ret;
        }

        [HttpGet()]
        [Route("{tenantId:Guid}")]
        public TenantModel GetTenant(Guid tenantId)
        {
            Guid environmentId = GetEnvironment();
            TenantModel ret = null;
            try
            {
                TableOperation retrieveOperation = TableOperation.Retrieve<TenantEntity>(environmentId.ToString(), tenantId.ToString());
                TableResult result = _tenantsTable.Execute(retrieveOperation);
                if (result.Result == null)
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                TenantEntity tenantEntity = (TenantEntity)result.Result;
                ret = new TenantModel(tenantEntity);
            }
            catch (Exception ex)
            {
                Log(environmentId, ex.Message);
                throw;
            }
            return ret;
        }

        [HttpPatch()]
        [Route("{tenantId:Guid}")]
        public TenantModel PatchTenant(Guid tenantId, [FromBody] IDictionary<string, JToken> arguments)
        {
            Guid environmentId = GetEnvironment();
            TenantModel ret = null;
            try
            {
                TableOperation retrieveOperation = TableOperation.Retrieve<TenantEntity>(environmentId.ToString(), tenantId.ToString());
                TableResult result = _tenantsTable.Execute(retrieveOperation);
                if (result.Result == null)
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                TenantEntity tenantEntity = (TenantEntity)result.Result;
                Dictionary<string, JToken> args = new Dictionary<string, JToken>(arguments, StringComparer.CurrentCultureIgnoreCase);
                JToken status = null;
                if (args.TryGetValue("status", out status))
                    tenantEntity.Status = status.Value<string>();
                TableOperation replaceOperation = TableOperation.Replace(tenantEntity);
                _tenantsTable.Execute(replaceOperation);
                ret = new TenantModel(tenantEntity);
            }
            catch (Exception ex)
            {
                Log(environmentId, ex.Message);
                throw;
            }
            return ret;
        }

        [HttpPost]
        [Route("")]
        public Guid PostTenant([FromBody] TenantModel tenant)
        {
            Guid environmentId = GetEnvironment();
            Guid ret = Guid.Empty;
            try
            {
                tenant.Id = Guid.NewGuid();
                TenantEntity tenantEntity = new TenantEntity(environmentId, tenant);
                TableOperation insertOperation = TableOperation.Insert(tenantEntity);
                _tenantsTable.Execute(insertOperation);
                ret = tenant.Id;
            }
            catch (Exception ex)
            {
                Log(environmentId, ex.Message);
                throw;
            }
            return ret;
        }

    }
}