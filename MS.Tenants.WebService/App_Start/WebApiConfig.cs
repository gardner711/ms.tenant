﻿using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System.Web.Http;

namespace MS.Tenants.WebService
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();
            CreateStorage();
        }

        public static void CreateStorage()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
                CloudConfigurationManager.GetSetting("StorageConnectionString"));
            CloudTableClient client = storageAccount.CreateCloudTableClient();
            CloudTable table;
            table = client.GetTableReference("tenants");
            table.CreateIfNotExists();
            table = client.GetTableReference("sharedkeys");
            table.CreateIfNotExists();

        }
    }
}
