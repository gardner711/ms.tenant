﻿using MS.Tenants.WebService.Entities;
using System;

namespace WAOnline.Tenants.WebService.Models
{
    public class SharedKeyModel
    {
        public SharedKeyModel() { }

        public SharedKeyModel(SharedKeyEntity sharedKey)
        {
            Id = sharedKey.Id;
            Name = sharedKey.Name;
            Description = sharedKey.Description;
            Key = sharedKey.Key;
            Disabled = sharedKey.Disabled;

        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Key { get; set; }
        public bool Disabled { get; set; }

    }
}
