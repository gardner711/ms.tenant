﻿using MS.Tenants.WebService.Entities;
using System;

namespace WAOnline.Tenants.WebService.Models
{
    public class TenantModel
    {
        public TenantModel() { }

        public TenantModel(TenantEntity tenant)
        {
            Id = tenant.Id;
            UrlFriendlyName = tenant.UrlFriendlyName;
            CompanyName = tenant.CompanyName;
            Status = tenant.Status;
            EnableServiceBus = Convert.ToBoolean(tenant.EnableServiceBus);

        }

        public Guid Id { get; set; }
        public string UrlFriendlyName { get; set; }
        public string CompanyName { get; set; }
        public string Status { get; set; }
        public bool EnableServiceBus { get; set; }

    }
}
