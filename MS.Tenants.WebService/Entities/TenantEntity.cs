﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using WAOnline.Tenants.WebService.Models;

namespace MS.Tenants.WebService.Entities
{
    public class TenantEntity : TableEntity
    {
        public TenantEntity() { }

        public TenantEntity(Guid environmentId, TenantModel tenant)
        {
            this.PartitionKey = environmentId.ToString();
            this.RowKey = tenant.Id.ToString();
            Id = tenant.Id;
            UrlFriendlyName = tenant.UrlFriendlyName;
            CompanyName = tenant.CompanyName;
            Status = tenant.Status;
            EnableServiceBus = tenant.EnableServiceBus;

        }

        public Guid Id { get; set; }
        public string UrlFriendlyName { get; set; }
        public string CompanyName { get; set; }
        public string Status { get; set; }
        public bool EnableServiceBus { get; set; }

    }
}