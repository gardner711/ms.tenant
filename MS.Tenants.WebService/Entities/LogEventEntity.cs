﻿namespace MS.Tenants.WebService.Entities
{
    public class LogEventEntity
    {
        public string Timestamp;

        public string Message;

        public string Tenant;

        public string TenantId;

        public string User;

        public string EndPoint;

        public string Environment;

        public string Machine;

        public string Category;

        public string Origin;
    }
}