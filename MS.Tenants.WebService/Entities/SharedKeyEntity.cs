﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using WAOnline.Tenants.WebService.Models;

namespace MS.Tenants.WebService.Entities
{
    public class SharedKeyEntity : TableEntity
    {
        public SharedKeyEntity() { }

        public SharedKeyEntity(Guid environmentId, Guid tenantId, SharedKeyModel sharedKey)
        {
            this.PartitionKey = environmentId.ToString() + "-" + tenantId.ToString();
            this.RowKey = sharedKey.Id.ToString();
            Id = sharedKey.Id;
            Name = sharedKey.Name;
            Description = sharedKey.Description;
            Key = sharedKey.Key;
            Disabled = sharedKey.Disabled;

        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Key { get; set; }
        public bool Disabled { get; set; }

    }
}